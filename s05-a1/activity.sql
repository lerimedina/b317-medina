SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName, email from employees WHERE lastName = "Patterson" and firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != ("USA") AND creditLimit > 3000;

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

SELECT employees.firstName, employees.lastName, offices.city 
FROM employees 
JOIN offices ON employees.officeCode = offices.officeCode 
WHERE offices.officeCode = 5;

SELECT customers.customerName 
FROM customers 
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber 
WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";

SELECT products.productName, customers.customerName 
FROM products 
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers on orders.customerNumber = customers.customerNumber
WHERE customers.customerName = "Baane Mini Imports";

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country 
FROM customers 
JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber 
JOIN offices ON offices.officeCode = employees.officeCode 
WHERE customers.country = offices.country;

SELECT products.productName, products.quantityInStock 
FROM products 
JOIN productlines ON products.productLine = productlines.productLine
WHERE products.productLine = "planes" AND products.quantityInStock < 1000;

SELECT customerName from customers WHERE phone LIKE "%+81%";